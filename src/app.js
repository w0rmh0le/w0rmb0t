'use strict';

const fs   = require('fs');
const path = require('path');

const discord = require('discord.js');

const imageGenerator = require('./modules/image-generator');


// Config //

const defaultConfigRequirePath = path.join(__dirname, '../w0rmb0t-config');
const defaultConfigFilePath = path.join(__dirname, '../w0rmb0t-config.js');

let configFilePath = process.argv[2];
let configRequirePath;

if (configFilePath
    && (configFilePath.indexOf('.js') !== -1)
    && fs.existsSync(configFilePath)
) {
    let index = configFilePath.indexOf('.js');
    configRequirePath = configFilePath.substr(0, index);
}
else if (configFilePath) {
    let message = `
Invalid config argument!

Create a config file named 'w0rmb0t-config.js' in 'w0rmb0t' directory,
or provide a valid argument for path to config.

Path can be absolute or relative to 'app.js'
`;

    console.log(message);
    process.exit(1);
}
else if (fs.existsSync(defaultConfigFilePath)) {
    configRequirePath = defaultConfigRequirePath;
}
else {
    let message = `
Could not find default config!

Create a config file named 'w0rmb0t-config.js' in 'w0rmb0t' directory,
or provide a valid argument for path to config.

Path can be absolute or relative to 'app.js'
`;

    console.log(message);
    process.exit(1);
}

const config = require(configRequirePath);

if (typeof config.token !== 'string' || typeof config.clientId !== 'string') {
    console.log('Config must contain a token and clientId!');
    process.exit(1);
}

// Start Bot //

const bot = new discord.Client();

bot.on('ready', () => {
    console.log('hello w0rms');
});

const playSoundbite = function (voiceChannel, filePath, textReply) {
    if (voiceChannel) {
        voiceChannel.join()
            .then (connection => {

                const dispatcher = connection.playFile(filePath);
                dispatcher.on('end', reason => {
                    voiceChannel.leave();
                    connection = null;
                });
            })
            .catch(console.error);
    }
    else {
        message.reply(textReply);
    }
};

bot.on('message', message => {

    if (message.author.bot) { return; }

    let text = message.content.substring(0).toLowerCase();

    // Voice chat stuff
    if (text === '!headshot') {
        const filePath = path.join(__dirname, 'assets', 'boomheadshot.mp3');
        const textReply = 'Boom Headshot!';
        playSoundbite(message.member.voiceChannel, filePath, textReply);
    }
    else if (text === '!hadouken') {
        const filePath = path.join(__dirname, 'assets', 'hadouken.mp3');
        const textReply = 'Hadouken!';
        playSoundbite(message.member.voiceChannel, filePath, textReply);
    }
    // end Voice chat stuff

    // this allows the bot to respond to @mentions
    if (message.isMentioned(config.clientId)) {
        if (text.indexOf('hello') !== -1) {
            message.reply('greetings fellow w0rm');
        }

        else {
            message.reply('I do not understand what you said. I only respond to "hello".');
        }
    }
});

imageGenerator.registerOnMessage(bot);

bot.login(config.token);

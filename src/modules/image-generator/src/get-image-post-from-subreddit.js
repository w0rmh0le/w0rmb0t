'use strict';

const { URL } = require('url');

const axios = require('axios');


/**
 * @typedef ImagePost
 * @property {string} url The url of the image
 * @property {string} title The title of the image
 * @property {string} permalink The permalink for the reddit post
 */

const imageExtensionsRegex = /\.*(bmp|gif|jpg|jpeg|mp4|png)$/i;

/**
 * Queries a given subreddit on reddit for a random image
 * @param {string} subredditName The name of the subreddit to query
 * @returns {ImagePost} imagePost
 * @throws {Error} error will have customCode property of
 * 'BAD_RESPONSE' or 'NO_IMAGES'
 */
module.exports = async (subredditName) => {
    const url = new URL('/r/' + subredditName + '/.json',
        'https://www.reddit.com');
    const response = await axios.get(url.toString());
    if (response.status !== 200) {
        let error = new Error('Bad response from http request.');
        error.customCode = 'BAD_RESPONSE';
        throw error;
    }

    const posts = response.data.data.children;
    let attemptedIndices = [];

    while (attemptedIndices.length < posts.length) {
        let i =  Math.floor(Math.random() * Math.floor(posts.length + 1));

        while (attemptedIndices.includes(i)) {
            i++;
        }
        if (i >= posts.length) {
            let error = new Error('Could not find an image in posts.');
            error.customCode = 'NO_IMAGES';
            throw error;
        }
        attemptedIndices.push(i);

        if (!posts[i].data.url.match(imageExtensionsRegex)) {
            continue;
        }

        /**
         * @type ImagePost
         */
        let post = {
            url: posts[i].data.url,
            title: posts[i].data.title,
            permalink: '<https://www.reddit.com' + posts[i].data.permalink + '>'
        };

        return post;
    }
};

'use strict';


exports.getImagePostFromSubreddit = require('./src/get-image-post-from-subreddit');

/**
 * Register an onMessage hook to the discord bot to parse get-image commands
 * @param {discord.js.Client} bot The Discord client for the bot
 */
exports.registerOnMessage = (bot) => {
    bot.on('message', (message) => {
        // Only parse math commands
        if (!message.content.toLowerCase().startsWith('!get-image')) {
            return;
        }

        const channel = message.channel;
        // Separate the command from options that may come after it.
        const command = (message.content.indexOf(' ') !== -1)
            ? message.content.substr(
                1, message.content.indexOf(' ') - 1)
                .toLowerCase()
            : message.content.substring(1).toLowerCase();
        const options = message.content.substr(message.content.indexOf(' ') + 1)
            .toLowerCase().split();

        switch (command) {
        case 'get-image-reddit':
            if (options.length !== 1) {
                message.reply(
                    'You must give the name of a subreddit!\n'
                    + 'e.g. "!get-image-reddit pics"');
            }
            const subreddit = options[0];

            this.getImagePostFromSubreddit(subreddit).then((imagePost) => {
                channel.send(':\n'
                    + imagePost.title
                    + '\n'
                    + imagePost.permalink
                    + '\n'
                    + imagePost.url);
            }).catch((err) => {
                let message = ':\n';
                switch (err.customCode) {
                case 'BAD_RESPONSE':
                    message += 'We got a bad response from the url.\n'
                        + 'Maybe you typed the subreddit wrong?';
                    break;
                case 'NO_IMAGES':
                    message += 'No images were found on that subreddit\n'
                        + 'Maybe try another?';
                    break;
                default:
                    message += 'Something went wrong, try again later.';
                    break;
                }

                channel.send(message);
            });
            break;

        default:
            message.reply('that is not a valid get-image command.\n'
                + 'Valid commands are:\n'
                + '`!get-image-reddit`');
            break;
        }
    });
};

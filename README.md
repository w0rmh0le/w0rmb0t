# w0rmb0t

Discord bot for teh w0rmh0le

## res0urces

- [Discord.js Docs](https://discord.js.org/#/docs/main/stable/general/welcome)
- [Creating a discord bot & getting a token](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token)

## adding features

When adding a new feature, create a new branch off of `develop`. Do your work in that branch, then make a pull request to `develop` when the feature is complete.

Features should be imported from a single file in the `src/modules/` directory. With this format, `src/app.js` can just `require` the feature from `src/modules/<feature_name>.js`.
If a feature becomes so large it requires multiple files then, for better organization, it may make sense to put create a directory for the feature inside `src/modules/`.
An example tree structure might be:
```bash
src/modules/
-----------/feature_foo/
-----------------------/feature_foo.js
-----------------------/lib_foo/
-------------------------------/bar.js
-------------------------------/baz.js
```
> I apologize for being terrible at representing directory structure with text.
